#!/bin/sh
# shellcheck disable=SC3043
# SC3043 (warning): In POSIX sh, 'local' is undefined
# Reason: Works with all current shells

# $0 is a script name, $1, $2, $3 etc are passed arguments
# $1 is our command

set -eu

# shellcheck disable=SC2142
# SC2142 (error): Aliases can't use positional parameters. Use a function.
alias info='echo >&2 "$(date -u -Is) ${0##*/}:${LINENO} ${FUNCNAME:-@main} INFO:"'

wait_for_db() {
  local port="${DATABASE_PORT:-5432}"
  until nc -z "${DATABASE_HOST}" "${port}" 1>/dev/null 2>&1; do
    info "Waiting for postgres (${DATABASE_HOST}:${port})"
    sleep 3
  done
  info "Connection to postgres successful (${DATABASE_HOST}:${port})"
}

setup_django() {
  info 'Run migrations'
  python manage.py migrate --noinput

  info 'Collect static files'
  python manage.py collectstatic --noinput

  info 'Create cache table'
  python manage.py createcachetable
}

setup_tests() {
  info 'Run migrations'
  python manage.py migrate --noinput

  info 'Create cache table'
  python manage.py createcachetable
}

cmd="$1"
shift

case "${cmd}" in
"setup-django")
  wait_for_db
  setup_django
  ;;

"devserver")
  wait_for_db

  info 'Start django development server'
  exec python manage.py runserver 0.0.0.0:8000
  ;;

"uwsgi")
  wait_for_db
  setup_django

  info 'Start django uwsgi server'
  exec uwsgi --ini /uwsgi/uwsgi.ini
  ;;

*)
  # Run custom command. Thanks to this line we can still use
  # "docker run our_container /bin/bash" and it will work
  exec "${cmd}" ${1+$@}
  ;;
esac
