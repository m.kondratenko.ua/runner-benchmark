ARG DNF_OPTS='-y \
--verbose \
--installroot /rootfs \
--releasever 9 \
--noplugins \
--config=/etc/dnf/dnf.conf \
--setopt=max_parallel_downloads=8 \
--setopt=reposdir=/etc/yum.repos.d \
--setopt=varsdir=/etc/dnf/vars \
--setopt=install_weak_deps=0 \
--setopt=tsflags=nodocs \
--setopt=override_install_langs=en_US.utf8'

FROM docker.io/library/almalinux:9.3 as almalinux

FROM almalinux as rootfs
ARG DNF_OPTS
COPY epel.repo /etc/yum.repos.d/epel.repo
COPY epel.gpg /etc/pki/rpm-gpg/RPM-GPG-KEY-EPEL-9
RUN dnf install ${DNF_OPTS} \
    glibc-minimal-langpack busybox python3.9 libstdc++ pcre-devel mailcap && \
    dnf clean all && \
    find /rootfs/usr/bin /rootfs/usr/sbin \( \
    \( -type f -or -type l \) \
    -and \
    \( \
    -not \( \
        -name 'ldconfig' \
        -or -name 'busybox' \
        -or -name 'python*' \
        \) \
      \) \
    \) -delete && \
    chroot /rootfs /usr/sbin/busybox --install -s && \
    # Remove pycache directories. Python cache takes up a considerable amount of space
    # and is removed by both official docker python image and google distroless python.
    find /rootfs -type d -name '__pycache__' -exec rm -rv {} +

FROM scratch as result
COPY --from=rootfs /rootfs /

FROM rootfs as build_rootfs
ARG DNF_OPTS
RUN dnf install ${DNF_OPTS} \
    gcc binutils python3.9-devel python3.9-pip && \
    dnf clean all && \
    ln -rs /usr/bin/ld.bfd /rootfs/usr/bin/ld

FROM scratch as python_dependencies
COPY --from=build_rootfs /rootfs /
COPY requirements.txt requirements.txt
RUN python3 -m venv /.venv && \
    PATH="/.venv/bin:${PATH}" VIRTUAL_ENV="/.venv" \
    pip install -r requirements.txt && \
    rm -rf /root/.cache ./requirements.txt

FROM scratch as runner
COPY --from=rootfs /rootfs /

RUN echo 'app:x:1000:' >> /etc/group && \
    echo 'app:x:1000:1000::/app:/sbin/nologin' >> /etc/passwd && \
    echo 'app:*:19347:0:99999:7:::' >> /etc/shadow && \
    mkdir /app && chown 1000:1000 /app && \
    mkdir /uwsgi && chown 1000:1000 /uwsgi

USER app
WORKDIR /app
ENV HOME=/app \
    PYTHONUNBUFFERED=1 \
    PYTHONPATH=/app \
    MEDIA_PATH=/app/media \
    STATIC_PATH=/app/static \
    SECRET_KEY=default \
    DJANGO_SETTINGS_MODULE=pipit.settings.prod \
    ALLOWED_HOSTS=* \
    PATH="/.venv/bin:${PATH}"

RUN mkdir -p "${MEDIA_PATH}" && \
    mkdir -p "${STATIC_PATH}"

COPY --chown=1000:1000 --from=python_dependencies /.venv /.venv
COPY --chown=0:0 /docker-entrypoint.sh /usr/local/bin/docker-entrypoint.sh
COPY --chown=1000:1000  uwsgi.ini /uwsgi/uwsgi.ini
COPY --chown=1000:1000 sample_django_app .

EXPOSE 8000
ENTRYPOINT ["docker-entrypoint.sh"]
CMD ["uwsgi"]
